

export const COURSES: any = {

    0: {
        id: 0,
        name: "Angular",
        description: "Advance Angular crash course with Rxjs",
    },

    1: {
        id: 1,
        name: "React",
       description: "Reach crash course",
    
    },
  
    2: {
        id: 2,
        name: 'Vue Js',
        description: "Vue Js crash course",
    },
    3: {
        id: 3,
        name: 'Laravel',
        description: "Laravel Crash course",
    },
 
  
};


export const STUDENTS = {
    0: {
        id: 1,
        "name": "John Stone",
        "studentID": "A1",
        courseId: 0
    },
    1: {
        id: 2,
        "name": "Ponnappa Priya",
        "studentID": "A2",
        courseId: 0
    },
    2: {
        id: 3,
        "name": "Mia Wong",
        "studentID":  "A3",
       courseId: 0
    },
    3: {
        id: 4,
        "name": "Peter Stanbridge",
        "studentID":  "A4",
       courseId: 0
    },
    4: {
        id: 5,
        "name": "Natalie Lee-Walsh",
        "studentID":  "A5",
       courseId: 0
    },
    5: {
        id: 6,
        "name": "Ang Li",
        "studentID":  "R1",
        courseId: 1
    },
    6: {
        id: 7,
        "name": "Nguta Ithya",
        "studentID": "R2",
        courseId: 1
    },
    7: {
        id: 8,
        "name": "Tamzyn French",
        "studentID": "R3",
        courseId: 1
    },
    8: {
        id: 9,
        "name": "Salome Simoes",
        "studentID": "R4",
        courseId: 1
    },
    9: {
        id: 10,
        "name": "Trevor Virtue",
        "studentID": "R5",
        courseId: 1
    },

    10: {
        id: 11,
        "name": "Tarryn Campbell-Gillies",
        "studentID": "V1",
        courseId: 2
    },

    11: {
        id: 12,
        "name": "Eugenia Anders",
        "studentID":"V2",
         courseId: 2
    },

    12: {
        id: 13,
        "name": "Andrew Kazantzis",
        "studentID": "V3",
         courseId: 2
    },

    13: {
        id: 14,
        "name": "Verona Blair",
        "studentID": "V4",
         courseId: 2
    },

    14: {
        id: 15,
        "name": "Jane Meldrum",
        "studentID":"V5",
         courseId: 2
    },
    15: {
        id: 16,
        "name": "Maureen M. Smith",
        "studentID": "L1",
        courseId: 3
    },
    16: {
        id: 17,
        "name": "Desiree Burch",
        "studentID":  "L2",
           courseId: 3
    },
    17: {
        id: 18,
        "name": "Daly Harry",
        "studentID":  "L3",
           courseId: 3
    },
    18: {
        id: 19,
        "name": "Hayman Andrews",
        "studentID":  "L4",
           courseId: 3
    },
    19: {
        id: 20,
        "name": "Ruveni Ellawala",
        "studentID":  "L5",
           courseId: 3
    },




};

export function findCourseById(courseId:number) {
    return COURSES[courseId];
}

export function findStudentsForCourse(courseId:number) {
    return Object.values(STUDENTS).filter(student => student.courseId == courseId);
}

