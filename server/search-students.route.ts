


import {Request, Response} from 'express';
import {STUDENTS} from "./db-data";
import {setTimeout} from "timers";



export function searchStudents(req: Request, res: Response) {

    const queryParams = req.query;

    const courseId = queryParams.courseId,
          filter = queryParams.filter || '',
          sortOrder = queryParams.sortOrder || 'asc',
          pageNumber = parseInt(queryParams.pageNumber) || 0,
          pageSize = parseInt(queryParams.pageSize) || 3;

    let students = Object.values(STUDENTS).filter(student => student.courseId == courseId).sort((l1, l2) => l1.id - l2.id);

    if (filter) {
        students = students.filter(student => student.name.trim().toLowerCase().search(filter.toLowerCase()) >= 0);
    }

    if (sortOrder == "desc") {
        students = students.reverse();
    }

    const initialPos = pageNumber * pageSize;

    const studentsPage = students.slice(initialPos, initialPos + pageSize);

    setTimeout(() => {
        res.status(200).json({payload: studentsPage});
    },1000);


}