

export interface Student {
    id: number;
    description: string;
    courseId: number;
}